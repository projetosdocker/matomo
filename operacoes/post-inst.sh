#!/bin/bash

####################################################################################
if [ "$1" = "-stack" ]; then
source operacoes/pre-step.sh
    if [ "$BRANCH" = "tag" ]; then
	echo "Executando dentro de producao"
        docker stack rm $CI_PROJECT_NAME
        docker stack deploy -c stack.yml $CI_PROJECT_NAME
    else
	echo "Executando fora de producao"
        docker stack rm $CI_PROJECT_NAME-$BRANCH_SISTEMA
        docker stack deploy -c stack.yml $CI_PROJECT_NAME-$BRANCH_SISTEMA
    fi
fi
####################################################################################
