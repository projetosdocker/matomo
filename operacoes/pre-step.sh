#!/bin/bash
#Arquivo de variaveis
source ./operacoes/var.sh

case $BRANCH in

	develop)
		# Mudanças no arquivo de Banco de Dados CASO PRECISAR
		#echo "Mudanças no arquivo de Banco de Dados"
		#sed -i 's/USER_BD/'"$USER_BD_HMG"'/'g ./$ARQUIVO_DS;
		#sed -i 's/PASS_BD/'"$PASS_BD_HMG"'/'g ./$ARQUIVO_DS;
		#sed -i 's/URLSERVICE/'"$URLSERVICEHMG"'/'g ./$ARQUIVO_DS;
		#sed -i 's/URLSSO/'"$URLSSOHMG"'/'g ./$ARQUIVO_DS;
		#sed -i 's/DBSISTEMA/'"$DBSISTEMAHMG"'/'g ./$ARQUIVO_DS;
		#cat ./$ARQUIVO_DS;
		
		# Mudança no stack
		sed -i 's/URL/'"$URLSERVICEHMG"'/'g ./stack.yml
		cat ./stack.yml
		;;

	tag)
		# Mudanças no arquivo de Banco de Dados CASO PRECISAR
		#echo "Mudanças no arquivo de Banco de Dados"
		#sed -i 's/USER_BD/'"$USER_BD"'/'g ./$ARQUIVO_DS;
		#sed -i 's/PASS_BD/'"$PASS_BD"'/'g ./$ARQUIVO_DS;
		#sed -i 's/URLSERVICE/'"$URLSERVICE"'/'g ./$ARQUIVO_DS;
		#sed -i 's/URLSSO/'"$URLSSO"'/'g ./$ARQUIVO_DS;
		#sed -i 's/DBSISTEMA/'"$DBSISTEMAHMG"'/'g ./$ARQUIVO_DS;
		#cat ./$ARQUIVO_DS;

		# Mudança no stack
		sed -i 's/URL/'"$URLSERVICE"'/'g ./stack.yml
		cat ./stack.yml
		;;
  esac
